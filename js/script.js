(function ($) {
    Drupal.behaviors.myModuleBehavior = {
        attach: function (context, settings) {

        //START SCRIPT

            //FUNCTION TEMPLATES

                //wowjs and animate.css
                function Animate_View_Rows(blockName, animationName){
                    $(blockName + " .view-content .views-row:nth-child(1)").addClass("wow animated " + animationName).attr("data-wow-delay","0.3s");
                    $(blockName + " .view-content .views-row:nth-child(2)").addClass("wow animated " + animationName).attr("data-wow-delay","0.6s");
                    $(blockName + " .view-content .views-row:nth-child(3)").addClass("wow animated " + animationName).attr("data-wow-delay","0.9s");
                    $(blockName + " .view-content .views-row:nth-child(4)").addClass("wow animated " + animationName).attr("data-wow-delay","0.9s");
                }
            
            //MAIN SCRIPT
            //Animate_View_Rows(".home-link", "flipInY");

        //END SCRIPT
        }
    };
})(jQuery);